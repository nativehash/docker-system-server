## Install

> Note the an UID and GID used on your system for the www user.
>
> Is important tu set correct users in .env file before run ```./install``` script.

First create preconfigured .env file:

```
./preset-env {project_domain} {app_git_clone_link}
```

Where {project_domain} is like: ```mydomain.com``` and {app_git_clone_link} include access token like: ```https://user:access_key@gitlab.com/nativehash/system.git```.


Check ```.env``` file and if is correct run:

```
./install
```

After installation, check ```app/.env``` file and make adjustments if necessary. You should change payment and email data.


On the end, you schould add admin user:

```sh
sudo docker-compose up -d php
./add-admin-user {name} {email} {password}
sudo docker-compose down
```

## Run

```
sudo docker-compose up
```

For standalone - not for webproxy - version run:

```
sudo docker-compose -f ./docker-compose-standalone.yml up
```

Stop:

```
sudo docker-compose down
```


## Tools

### App bash console

```
./app-bash
```

### Database console

```
app-database
```

### update app

```sh
./app-update
```

### Autostart

If script path is other than the default, change before copy.

```
sudo cp mdcms-server.service /etc/systemd/system/
sudo systemctl start mdcms-server && sudo systemctl enable mdcms-server
```

### Scheduler

Add to root crontab (check path and user before):
```
* * * * * cd /srv/system.nativehash.io && /usr/bin/docker-compose exec -T -u www-data php bash -c "cd /var/www && php artisan schedule:run >> /dev/null 2>&1"
```

### Backup

During backup create and restore, database docker service must be online.

#### Create backup

```
./make-backup
```

Will create a backup with the date as the name. If you want to set your own name, give it as an argument:

```
./make-backup copy_1
```

Backup creates a copy of app database and public storeage files.

#### Restore backup

```
./restore-backup backup_name
```

#### Auto backup

Example for weekly backup.
Add to root crontab:
```
59 23 * * * /srv/system.nativehash.io/make-backup $(date +\%A) >> /dev/null
```
