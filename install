#!/bin/bash

random_pass () {
    echo $(tr -dc A-Za-z0-9 </dev/urandom | head -c $1)
}

if [ ! -f ".env" ]; then
    echo ".env file not exists!"
    exit 1
fi

source .env

git clone $APP_GIT_CLONE_LINK $PROJECT_ROOT

echo "Change storage dir permissions..."
sudo chmod -R 777 "$PROJECT_ROOT/storage"
cd app
git restore *.gitignore
cd ..

echo "Configure .env file..."
cp "$PROJECT_ROOT/.env.example" "$PROJECT_ROOT/.env"
sed -i 's|DB_HOST=.*|DB_HOST=systemdb|g' "$PROJECT_ROOT/.env"
sed -i 's|DB_DATABASE=.*|DB_DATABASE=project|g' "$PROJECT_ROOT/.env"
sed -i 's|DB_USERNAME=.*|DB_USERNAME=db_user|g' "$PROJECT_ROOT/.env"
sed -i "s|DB_PASSWORD=.*|DB_PASSWORD=$DB_PASSWORD|g" "$PROJECT_ROOT/.env"
sed -i "s|APP_URL=.*|APP_URL=https://$PROJECT_DOMAIN|g" "$PROJECT_ROOT/.env"
sed -i 's|APP_DEBUG=.*|APP_DEBUG=false|g' "$PROJECT_ROOT/.env"
sed -i 's|APP_ENV=.*|APP_ENV=production|g' "$PROJECT_ROOT/.env"
sed -i "s|APP_KEY=|APP_KEY=$(random_pass 32)|g" "$PROJECT_ROOT/.env"

sudo chown -R $WWW_DATA_UID:$WWW_DATA_GID "$PROJECT_ROOT"

sudo docker-compose -f ./docker-compose-standalone.yml build php
sudo docker-compose -f ./docker-compose-standalone.yml up -d php
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php ln -s ../storage/app/public ./public/storage
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php composer install --no-interaction --prefer-source --optimize-autoloader --no-dev
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php php artisan migrate --force
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php php artisan db:seed --class=ChannelsTypesSeeder --force
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php php artisan db:seed --class=SitesSeeder --force
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php php artisan db:seed --class=ArchetypesSeeder --force
sudo docker-compose -f ./docker-compose-standalone.yml exec -w /var/www -u $WWW_DATA_UID php php artisan jwt:secret
sudo docker-compose -f ./docker-compose-standalone.yml stop

echo 'Done.'
